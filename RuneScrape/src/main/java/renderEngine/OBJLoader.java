package renderEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import models.RawModel;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class OBJLoader {

    /**
     *
     * Loads data from obj file. Order verticies, textures, normals and indicies
     * into order and return rawmodel of it.
     *
     * @param fileName
     * @return RawModel
     * @throws FileNotFoundException
     */
    public static ArrayList<RawModel> loadObjModel(Loader loader, String fileName) throws FileNotFoundException, IOException {

        FileReader fr = null;
        try {
            fr = new FileReader(new File("res/" + fileName + ".obj"));
        } catch (FileNotFoundException e) {
            System.err.println("Could'nt load the file!");
            e.printStackTrace();
        }

        BufferedReader bufferReader = new BufferedReader(fr);
        
        ArrayList<RawModel> modelArray = new ArrayList<RawModel>();
        
        String line = null;
        List<Vector3f> verticies = new ArrayList<Vector3f>();
        List<Vector2f> textures = new ArrayList<Vector2f>();
        List<Vector3f> normales = new ArrayList<Vector3f>();
        List<Integer> indicies = new ArrayList<Integer>();

        float[] verticiesArray = null;
        float[] texturesArray = null;
        float[] normalsArray = null;
        int[] indiciesArray = null;

        while (true) {
            try {
                while (true) {
                    line = bufferReader.readLine();
                    String[] currentLine = line.split(" ");
                    // checks what the line of the obj file means
                    if (line.startsWith("v ")) {
                        Vector3f vertex = new Vector3f(Float.parseFloat(currentLine[1]),
                                Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));

                        verticies.add(vertex);
                    } else if (line.startsWith("vt ")) {

                        Vector2f texture = new Vector2f(Float.parseFloat(currentLine[1]),
                                Float.parseFloat(currentLine[2]));

                        textures.add(texture);

                    } else if (line.startsWith("vn ")) {
                        Vector3f normal = new Vector3f(Float.parseFloat(currentLine[1]),
                                Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));

                        normales.add(normal);
                    } else if (line.startsWith("f ")) {
                        texturesArray = new float[verticies.size() * 2];
                        normalsArray = new float[verticies.size() * 3];
                        break;
                    }
                }

                while (line != null) {
                    if (!line.startsWith("f ")) {
                        break;
                    }

                    String[] currentLine = line.split(" ");
                    String[] vertex1 = currentLine[1].split("/");
                    String[] vertex2 = currentLine[2].split("/");
                    String[] vertex3 = currentLine[3].split("/");

                    processArray(vertex1, indicies, textures, normales, normalsArray, texturesArray);
                    processArray(vertex2, indicies, textures, normales, normalsArray, texturesArray);
                    processArray(vertex3, indicies, textures, normales, normalsArray, texturesArray);

                    line = bufferReader.readLine();
                }

                //bufferReader.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

            verticiesArray = new float[verticies.size() * 3];
            indiciesArray = new int[indicies.size()];

            int vertexPointer = 0;
            for (Vector3f vertex : verticies) {
                verticiesArray[vertexPointer++] = vertex.x;
                verticiesArray[vertexPointer++] = vertex.y;
                verticiesArray[vertexPointer++] = vertex.z;
            }

            for (int i = 0; i < indicies.size(); ++i) {
                indiciesArray[i] = indicies.get(i);
            }
            modelArray.add(loader.loadToVAO(verticiesArray, texturesArray, normalsArray, indiciesArray));
            if(line == null) {
                break;
            }
        }
        bufferReader.close();
        return modelArray;
    }

    /**
     *
     * Sorts out textures, indicies and normals and puts them into correct
     * order.
     *
     * @param data
     * @param indicies
     * @param textures
     * @param normals
     * @param normalArray
     * @param textureArray
     */
    static void processArray(String[] data, List<Integer> indicies, List<Vector2f> textures,
            List<Vector3f> normals, float[] normalArray, float[] textureArray) {

        int currentVertexPointer = Integer.parseInt(data[0]) - 1;
        indicies.add(currentVertexPointer);

        Vector2f textureVertex = textures.get(Integer.parseInt(data[1]) - 1);
        textureArray[currentVertexPointer * 2] = textureVertex.x;
        textureArray[currentVertexPointer * 2 + 1] = 1 - textureVertex.y;     
        //1 - textureVertex.y because opengl has 0,0 in the top left corner,
        //not in the down left
        int normalPointer = Integer.parseInt(data[2]) - 1;
        normalArray[currentVertexPointer * 3] = normals.get(normalPointer).x;
        normalArray[currentVertexPointer * 3 + 1] = normals.get(normalPointer).y;
        normalArray[currentVertexPointer * 3 + 2] = normals.get(normalPointer).z;
    }

}
