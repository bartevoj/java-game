package renderEngine;

import entities.Camera;
import entities.Entity;
import entities.Light;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import models.TexturedModel;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import shaders.StaticShader;
import shaders.TerrainShader;
import terrains.Terrain;

/**
 * Optimized renderer, uses Hash map with keys as texture models and values are
 * entites using the key texture, thanks to this there is only one bidnding and
 * unbinding per texture per frame instead of each object having unique texture
 *
 * @author vojta
 */
public class MasterRenderer {

    private static final float FOV = 70;
    private static final float NEAR_PLANE = 0.1f;
    private static final float FAR_PLANE = 1000;

    private Matrix4f projectionMatrix;

    private StaticShader shader = new StaticShader();
    private EntityRenderer renderer;

    private TerrainRenderer terrainRenderer;
    private TerrainShader terrainShader = new TerrainShader();

    private Map<TexturedModel, List<Entity>> entities = new HashMap<TexturedModel, List<Entity>>();
    // Hash map/dictionary with keys as texture models and values are objects/entites
    // using this texture

    private List<Terrain> terrains = new ArrayList<Terrain>();

    public MasterRenderer() {
        enableCulling();
        // backwards (away from camera) - optimization

        createProjectionMatrix();
        renderer = new EntityRenderer(shader, projectionMatrix);
        terrainRenderer = new TerrainRenderer(terrainShader, projectionMatrix);
    }

    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }
    
    public static void enableCulling(){
        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_BACK); // stops rendering of vertices facing 
    }
    
    public static void disableCulling(){
        GL11.glDisable(GL11.GL_CULL_FACE);
    }

    /**
     * renderes all the entities in our hash map according to the position of
     * the camera and the sun
     *
     * @param sun
     * @param camera
     */
    public void render(Light sun, Camera camera) {
        prepare();
        shader.start();
        shader.loadLight(sun);
        shader.loadViewMatrix(camera);
        renderer.render(entities);
        shader.stop();
        terrainShader.start();
        terrainShader.loadLight(sun);
        terrainShader.loadViewMatrix(camera);
        terrainRenderer.render(terrains);
        terrainShader.stop();
        terrains.clear();
        entities.clear(); // we have to clear the map each frame otherwise we would
        // keep adding entities above othe entities
    }

    /**
     * adds entity to our hash map and links it to its texture model
     *
     * @param entity - entity to be rendered
     */
    public void processEntity(Entity entity) {
        TexturedModel entityModel = entity.getModel();
        List<Entity> batch = entities.get(entityModel);
        if (batch != null) {
            batch.add(entity);
        } else {
            List<Entity> newBatch = new ArrayList<Entity>();
            newBatch.add(entity);
            entities.put(entityModel, newBatch);
        }
    }

    /**
     * Runs every frame of game loop, prepares OpenGL to render
     */
    public void prepare() {
        //renders according to depth of the object 
        //(for example does not render objects that are on top of each other)

        GL11.glEnable(GL11.GL_DEPTH_TEST);
        // Clears colors and depth from last frame
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(0.1f, 0.3f, 0.5f, 1f); // Sets background color to red

    }

    public void processTerrain(Terrain terrain) {
        terrains.add(terrain);
    }

    /**
     * creates projection matrix according to static parameters: field of view,
     * near plane, far plane
     */
    private void createProjectionMatrix() {
        float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
        float y_scale = (float) ((1f / Math.tan(Math.toRadians(FOV / 2f))) * aspectRatio);
        float x_scale = y_scale / aspectRatio;
        float frustum_length = FAR_PLANE - NEAR_PLANE;

        projectionMatrix = new Matrix4f();
        projectionMatrix.m00 = x_scale;
        projectionMatrix.m11 = y_scale;
        projectionMatrix.m22 = -((FAR_PLANE + NEAR_PLANE) / frustum_length);
        projectionMatrix.m23 = -1;
        projectionMatrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / frustum_length);
        projectionMatrix.m33 = 0;
    }

    public void cleanUp() {
        shader.cleanUp();
        terrainShader.cleanUp();
    }
}
