package renderEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import models.RawModel;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author vojta
 */
public class OBJLoaderSingle {

    /**
     *
     * Loads data from obj file. Order verticies, textures, normals and indicies
     * into order and return rawmodel of it.
     *
     * @param loader
     * @param fileName
     * @return RawModel
     * @throws FileNotFoundException
     */
    public static RawModel loadObjModel(Loader loader, String fileName) throws FileNotFoundException {

        FileReader fr = null;
        try {
            fr = new FileReader(new File("res/" + fileName + ".obj"));
        } catch (FileNotFoundException e) {
            System.err.println("Could'nt load the file!");
            e.printStackTrace();
        }

        BufferedReader bufferReader = new BufferedReader(fr);
        String line = null;
        List<Vector3f> verticies = new ArrayList<Vector3f>();
        List<Vector2f> textures = new ArrayList<Vector2f>();
        List<Vector3f> normales = new ArrayList<Vector3f>();
        List<ArrayList<String[]>> indicies = new ArrayList<ArrayList<String[]>>();

        float[] verticiesArray = null;
        float[] texturesArray = null;
        float[] normalsArray = null;
        int[] indiciesArray = null;

        try {
            line = bufferReader.readLine();
            while (line != null) {

                String[] currentLine = line.split(" ");
                // checks what the line of the obj file means
                if (line.startsWith("v ")) {
                    Vector3f vertex = new Vector3f(Float.parseFloat(currentLine[1]),
                            Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));

                    verticies.add(vertex);
                } else if (line.startsWith("vt ")) {

                    Vector2f texture = new Vector2f(Float.parseFloat(currentLine[1]),
                            Float.parseFloat(currentLine[2]));

                    textures.add(texture);

                } else if (line.startsWith("vn ")) {
                    Vector3f normal = new Vector3f(Float.parseFloat(currentLine[1]),
                            Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));

                    normales.add(normal);
                } else if (line.startsWith("f ")) {
                    ArrayList face = new ArrayList<String[]>();
                    String[] vertex1 = currentLine[1].split("/");                  
                    String[] vertex2 = currentLine[2].split("/");
                    String[] vertex3 = currentLine[3].split("/");
                    face.add(vertex1);
                    face.add(vertex2);
                    face.add(vertex3);
                    indicies.add(face);
                }

                line = bufferReader.readLine();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Integer> vertexIndicies = new ArrayList<Integer>();
        texturesArray = new float[verticies.size() * 2];
        normalsArray = new float[verticies.size() * 3];

        for (int faceLine = 0; faceLine < indicies.size(); ++faceLine) {
            for (int i = 0; i < 3; ++i) {               
                String[] data = indicies.get(faceLine).get(i);
                //System.out.println(Arrays.toString(data));
                int currentVertexPointer = Integer.parseInt(data[0]) - 1;
                // -1 because obj indentation starts at 1 not at 0
                vertexIndicies.add(currentVertexPointer);

                Vector2f textureVertex = textures.get(Integer.parseInt(data[1]) - 1);
                texturesArray[currentVertexPointer * 2] = textureVertex.x;
                texturesArray[currentVertexPointer * 2 + 1] = 1 - textureVertex.y;
                // 1 - textureVertex.y because opengl has 0,0 in the top left corner,
                // not in down left

                int normalPointer = Integer.parseInt(data[2]) - 1;
                normalsArray[currentVertexPointer * 3] = normales.get(normalPointer).x;
                normalsArray[currentVertexPointer * 3 + 1] = normales.get(normalPointer).y;
                normalsArray[currentVertexPointer * 3 + 2] = normales.get(normalPointer).z;
            }
        }
        
        
        verticiesArray = new float[verticies.size() * 3];
        
        int vertexPointer = 0;
        for (Vector3f vertex : verticies) {
            verticiesArray[vertexPointer++] = vertex.x;
            verticiesArray[vertexPointer++] = vertex.y;
            verticiesArray[vertexPointer++] = vertex.z;
        }
        
        indiciesArray = new int[vertexIndicies.size()];
        
        for (int i = 0; i < vertexIndicies.size(); ++i) {
            indiciesArray[i] = vertexIndicies.get(i);
        }
        
        return loader.loadToVAO(verticiesArray, texturesArray, normalsArray, indiciesArray);
        
    }
}
