package renderEngine;

import models.RawModel;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

/**
 * Each 3D graphical model is represented by a series of triangles, each
 * triangle has 3 vertices. VAO - vertex array object, is an object that
 * contains attribute lists, which can store vertex data (vertex
 * positions/colors) and buffer objects(VBO). VBO - vertex buffer object, is an
 * object that stores vertex array data.
 */
public class Loader {

    private List<Integer> vaos = new ArrayList<Integer>();
    private List<Integer> vbos = new ArrayList<Integer>();
    private List<Integer> textures = new ArrayList<Integer>();

    /**
     * Creates a VAO, stores @param positions into an attribute list
     *
     * @param positions The coordinates of all triangles that represent a model
     * @param indices The order in which vertices are to be rendered
     * @return
     */
    public RawModel loadToVAO(float[] positions, float[] textureCoords,
            float[] normals, int[] indices) {
        int vaoID = createVAO();
        bindIndiciesBuffer(indices);
        storeDataInAttributeList(0, 3, positions);
        storeDataInAttributeList(1, 2, textureCoords); //stores texture pos into 2. element of VAO
        storeDataInAttributeList(2, 3, normals);
        unbindVAO();
        return new RawModel(vaoID, indices.length);

    }

    /**
     * loads texture into memory and returns id of it
     *
     * @param fileName
     * @return texture id
     */
    public int loadTexture(String fileName) throws IOException {

        Texture texture = null;

        try {
            texture = TextureLoader.getTexture("PNG", new FileInputStream("res/" + fileName + ".png"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Loader.class.getName()).log(Level.SEVERE, null, ex);
        }

        int textureID = texture.getTextureID();
        textures.add(textureID);

        return textureID;
    }

    /**
     * deletes all the VAOs, VBOs, textures after closing the window
     */
    public void cleanUp() {
        for (int vao : vaos) {
            GL30.glDeleteVertexArrays(vao);
        }
        for (int vbo : vbos) {
            GL15.glDeleteBuffers(vbo);
        }
        for (int texture : textures) {
            GL11.glDeleteTextures(texture);
        }

    }

    private int createVAO() {
        int vaoID = GL30.glGenVertexArrays(); // Creates empty VAO and returns it's ID
        vaos.add(vaoID);
        GL30.glBindVertexArray(vaoID); // Activates the VAO
        return vaoID;
    }

    private void storeDataInAttributeList(int attributeNumber, int coordSize, float[] data) {
        int vboID = GL15.glGenBuffers(); // Creates empty VBO
        vbos.add(vboID);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID); // Activates the VBO
        // GL_ARRAY_BUFFER is the type of our current VBO

        FloatBuffer buffer = storeDataInFloatBuffer(data);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
        // Puts float buffer into VBO
        // GL15.GL_STATIC_DRAW means the data will not change, it will only be used for visualisation

        GL20.glVertexAttribPointer(attributeNumber, coordSize, GL11.GL_FLOAT, false, 0, 0);
        // Puts our VBO into the VAO on position attributeNumber
        // 3 because each vertex has 3 coordinates, GL_FLOAT => type of data, false => not normalized,
        // 0 => no other data between verticies, 0 => no offset - start from the beggining

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0); // Unbind the current VBO
    }

    private void unbindVAO() {
        GL30.glBindVertexArray(0); // This will unbind currently active VAO
    }

    /*
        Creates a VBO buffer, which is going to contain list of indicies
        this will be used with our vertices, list of indicies contains the 
        order in which vertices are going to be rendered, this lowers the number
        of vertices. Example, say we want to create an quad made from two 
        triangles. Each triangle is made out of 3 vertices, but we only need
        4 vertices (points) to draw a quad - there will be two pairs of duplicate
        vertices, if we use only 4 (unique) vertices and we specifie the order
        (in list of indicies), the result will be the same and in more complex
        cases we will save memory and time
        
     */
    private void bindIndiciesBuffer(int[] indicies) {

        int vboID = GL15.glGenBuffers();
        vbos.add(vboID);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
        IntBuffer buffer = storeDataInIntBuffer(indicies);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
    }

    private IntBuffer storeDataInIntBuffer(int[] data) {
        IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        return buffer;
    }

    private FloatBuffer storeDataInFloatBuffer(float[] data) {
        // Converts float array into a buffer
        FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data);
        buffer.flip(); // This tells the buffer that we have finished writing data into it and we want to it to output data
        return buffer;
    }
}
