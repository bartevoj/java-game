package renderEngine;

import entities.Camera;
import entities.Entity;
import java.util.List;
import java.util.Map;
import models.RawModel;
import models.TexturedModel;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import shaders.StaticShader;
import textures.ModelTexture;
import tools.GameController;
import tools.Maths;

/**
 * Class for rendering
 */
public class EntityRenderer {

    private StaticShader shader;

    /**
     * loads projection matrix to the shader
     *
     * @param shader
     */
    public EntityRenderer(StaticShader shader, Matrix4f projectionMatrix) {
        this.shader = shader;
        shader.start();
        shader.loadProjectionMatrix(projectionMatrix);
        shader.stop();
    }

    /**
     * Renders the inputted entity with its shader
     *
     * @param entities hash map with keys being texture models and values are
     * lists of entities using this texture model
     */
    public void render(Map<TexturedModel, List<Entity>> entities) {
        for (TexturedModel model : entities.keySet()) {
            prepareTexturedModel(model);
            List<Entity> batch = entities.get(model);
            for (Entity entity : batch) {
                prepareInstance(entity);
                GL11.glDrawElements(GL11.GL_TRIANGLES,
                        model.getRawModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
            }
            unbindTexturedModel();
        }
    }

    /**
     * prepares model to be textured
     * @param model 
     */
    private void prepareTexturedModel(TexturedModel model) {
        RawModel rawModel = model.getRawModel();
        GL30.glBindVertexArray(rawModel.getVaoID()); // activates VAO
        GL20.glEnableVertexAttribArray(0); // activates the attribute 
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);

        ModelTexture texture = model.getTexture();
        if (texture.isHasTransparency()){
            MasterRenderer.disableCulling();
        }
        shader.loadFakeLightningVariable(texture.isUseFakeLightning());
        shader.loadShineVariables(texture.getShineDamper(), texture.getReflectivity());
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, model.getTexture().getTextureID());
    }

    private void unbindTexturedModel() {
        MasterRenderer.enableCulling();
        GL20.glDisableVertexAttribArray(0); // disables the attribute list
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);
        GL30.glBindVertexArray(0); // deactivates VAO
    }
    /**
     * Prepares model position to be rendered
     */
    private void prepareInstance(Entity entity) {
        Matrix4f transformationMatrix;

        if (entity.isIsPlayer()){
            entity.setPosition(GameController.getPlayerPos());
        }

        transformationMatrix = Maths.createTransformationMatrix(entity.getPosition(),
                        entity.getRotX(), entity.getRotY(), entity.getRotZ(), entity.getScale());

        shader.loadTransformationMatrix(transformationMatrix);
    }

}
