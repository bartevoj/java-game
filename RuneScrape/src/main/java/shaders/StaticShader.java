package shaders;

import entities.Camera;
import entities.Light;
import org.lwjgl.util.vector.Matrix4f;
import tools.Maths;

/**
 *
 *
 */
public class StaticShader extends ShaderProgram {

    private static final String VERTEX_FILE = "src/main/java/shaders/vertexShader.txt";
    private static final String FRAGMENT_FILE = "src/main/java/shaders/fragmentShader.txt";

    private int transformationMatrix_location;
    private int projectionMatrix_location;
    private int viewMatrix_location;
    private int lightPosition_location;
    private int lightColor_location;
    private int shineDamper_location;
    private int reflectivity_location;
    private int useFakeLightning_location;

    public StaticShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");     //binds position to shaderProgram
        super.bindAttribute(1, "texturePos");   //binds texture to shaderProgram
        super.bindAttribute(2, "normal");
    }

    @Override
    protected void getAllUniformLocations() {
        transformationMatrix_location
                = super.getUniformLocation("transformationMatrix");
        projectionMatrix_location
                = super.getUniformLocation("projectionMatrix");
        viewMatrix_location
                = super.getUniformLocation("viewMatrix");
        lightPosition_location
                = super.getUniformLocation("lightPosition");
        lightColor_location
                = super.getUniformLocation("lightColor");
        shineDamper_location
                = super.getUniformLocation("shineDamper");
        reflectivity_location
                = super.getUniformLocation("reflectivity");
        useFakeLightning_location
                = super.getUniformLocation("useFakeLightning");
    }

    public void loadFakeLightningVariable(boolean useFake){
        super.loadBoolean(useFakeLightning_location, useFake);
    }
    
    public void loadShineVariables(float damper, float reflectivity) {
        super.loadFloat(shineDamper_location, damper);
        super.loadFloat(reflectivity_location, reflectivity);
    }

    public void loadTransformationMatrix(Matrix4f matrix) {
        super.loadMatrix(transformationMatrix_location, matrix);

    }

    public void loadProjectionMatrix(Matrix4f projection) {
        super.loadMatrix(projectionMatrix_location, projection);

    }

    public void loadViewMatrix(Camera camera) {
        Matrix4f matrix = Maths.createViewMatrix(camera);
        super.loadMatrix(viewMatrix_location, matrix);
    }

    public void loadLight(Light light) {
        super.loadVector(lightPosition_location, light.getPosition());
        super.loadVector(lightColor_location, light.getColor());
    }

}
