package textures;

/**
 *
 * Information about texture of a model
 */
public class ModelTexture {

    private final int textureID;
    private float shineDamper = 1; // represents how close the camera needs to be
    // to the reflected light to be able to capture it
    private float reflectivity = 0; // represents how much light will be reflected

    private boolean hasTransparency = false;
    private boolean useFakeLightning = false;

    public boolean isUseFakeLightning() {
        return useFakeLightning;
    }

    public void setUseFakeLightning(boolean useFakeLightning) {
        this.useFakeLightning = useFakeLightning;
    }
    
    public float getShineDamper() {
        return shineDamper;
    }

    public boolean isHasTransparency() {
        return hasTransparency;
    }

    public void setHasTransparency(boolean hasTransparency) {
        this.hasTransparency = hasTransparency;
    }
    

    public float getReflectivity() {
        return reflectivity;
    }

    public void setShineDamper(float shineDamper) {
        this.shineDamper = shineDamper;
    }

    public void setReflectivity(float reflectivity) {
        this.reflectivity = reflectivity;
    }

    public ModelTexture(int textureID) {
        this.textureID = textureID;
    }

    public int getTextureID() {
        return textureID;
    }

}
