package models;

import textures.ModelTexture;

/**
 *
 * Info about both model and its texture
 */
public class TexturedModel {

    private final RawModel rawModel;
    private final ModelTexture texture;

    public TexturedModel(RawModel rawModel, ModelTexture texture) {
        this.rawModel = rawModel;
        this.texture = texture;
    }

    public RawModel getRawModel() {
        return rawModel;
    }

    public ModelTexture getTexture() {
        return texture;
    }

}
