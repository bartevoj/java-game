package entities;

import models.TexturedModel;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * Entity represents everything about model
 *
 * -texture, position, rotation, ...
 *
 * @author bosadam
 */
public class Entity {

    private TexturedModel model;
    private float rotX, rotY, rotZ;
    private Vector3f position;
    private float scale;
    private boolean isPlayer = false;
    
    /**
     *
     * @param model
     * @param rotX
     * @param rotY
     * @param rotZ
     * @param position
     * @param scale
     */
    public Entity(TexturedModel model, float rotX, float rotY, float rotZ, Vector3f position, float scale) {
        this.model = model;
        this.rotX = rotX;
        this.rotY = rotY;
        this.rotZ = rotZ;
        this.position = position;
        this.scale = scale;
    }

    public boolean isIsPlayer() {
        return isPlayer;
    }

    public void setIsPlayer(boolean isPlayer) {
        this.isPlayer = isPlayer;
    }
    
    public void increasePosition(float x, float y, float z) {
        position.x += x;
        position.y += y;
        position.z += z;
    }

    public void increaseRotation(float rotx, float roty, float rotz) {
        rotX += rotx;
        rotY += roty;
        rotZ += rotz;

    }

    public TexturedModel getModel() {
        return model;
    }

    public float getRotX() {
        return rotX;
    }

    public float getRotY() {
        return rotY;
    }

    public float getRotZ() {
        return rotZ;
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getScale() {
        return scale;
    }

    public void setModel(TexturedModel model) {
        this.model = model;
    }

    public void setRotX(float rotX) {
        this.rotX = rotX;
    }

    public void setRotY(float rotY) {
        this.rotY = rotY;
    }

    public void setRotZ(float rotZ) {
        this.rotZ = rotZ;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

}
