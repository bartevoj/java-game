package entities;

import java.util.ArrayList;

/**
 * Class representing model combined of multiple models and textures
 * @author vojta
 */
public class ModelOfEntities {
    
    private ArrayList<Entity> entities;

    public ModelOfEntities(ArrayList<Entity> entities) {
        this.entities = entities;
    }

    public ArrayList<Entity> getEntities() {
        return entities;
    }
    
}
