package entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import tools.GameController;
import tools.Maths;
import tools.Sphere;

public class Camera {

    private Vector3f position;
    private float pitch = -60f;    //rotation of the camera around x axis
    private float yaw = 0;      //rotation of the camera around y axis
    private float roll;     //rotation of the camera around z axis

    private float UPPER_ANGLE = 75f;
    private float LOWER_ANGLE = 0f;
    
    private float radius = 100f;
    private float Theta = 30;
    private float FI = 0;
    
    private Light light;
    private Vector3f lightPos;
    private Matrix4f originalPos;
    
    public Camera(Light light, ModelOfEntities player){
        position = new Vector3f(Sphere.getCoordsOnSphere(player.getEntities().get(0).getPosition(), radius, Theta, FI));
        // changed to get position of first entity in player model
        this.light = light;
        lightPos = new Vector3f(position.x, light.getPosition().y, position.z);
        light.setPosition(lightPos);
        originalPos = Maths.createPlayerPOSViewMatrix(this);
    }
    /**
     * Moves camera in the direction. Light should move with it.
     * @param direction 
     */
    public void moveTowards(Vector3f direction){
        direction.normalise();
        position.x += direction.x;
        position.y += direction.y;
        position.z += direction.z;
        
        lightPos.x += direction.x;
        lightPos.y += direction.y;
        lightPos.z += direction.z;
        light.setPosition(lightPos);
    }
    
    /**
     * scans for an input
     */
    public void move() {
        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            if (Theta > LOWER_ANGLE){
                Theta -= 0.5f;
                pitch -= 0.5f;
                setCameraPosition();
            }
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            if (Theta < UPPER_ANGLE){
                Theta += 0.5f;
                pitch += 0.5f;
                setCameraPosition();
            }
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            FI -= 0.8f;
            yaw -= 0.8f;
            setCameraPosition();
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            FI += 0.8f;
            yaw += 0.8f;
            setCameraPosition();
        }
        
        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
            position.y += 1f;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_F)) {
            position.y -= 1f;
        }
        
        if (Keyboard.isKeyDown(Keyboard.KEY_E)) {
            //position.x += 0.02f;
            yaw -= 0.5f;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_Q)) {
            //position.x -= 0.02f;
            yaw += 0.5f;
        }

    }
    /**
     * Sets position on the sphere.
     * @param controller 
     */
    private void setCameraPosition(){
        Theta = Theta % 360;
        FI = FI % 360;
        Vector3f direction = Sphere.getCoordsOnSphere(GameController.getPlayerPos(), radius, Theta, FI);
        position.x = direction.x;
        position.y = direction.y;
        position.z = direction.z;
        }

    public Matrix4f getOriginalPos() {
        return originalPos;
    }
    
    public Vector3f getPosition() {
        return position;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public float getRoll() {
        return roll;
    }

    public void setOriginalPos(Matrix4f originalPos) {
        this.originalPos = originalPos;
    }
    
}
