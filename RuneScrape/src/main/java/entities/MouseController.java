package entities;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import terrains.Terrain;
import tools.Maths;

public class MouseController {

    private Vector3f currentRay;
    private Matrix4f projectionMatrix;
    private Matrix4f viewMatrix;
    private Camera camera;
    
        //precision of the terrain point
    private static final int RECURSION_COUNT = 500;
    private static final float RAY_RANGE = 600;
    private Terrain terrain;
    private Vector3f currentTerrainPoint;
    
    private boolean clicked = false;
    
    public MouseController(Matrix4f projectionMatrix, Camera cam, Terrain terrain){
        this.projectionMatrix = projectionMatrix;
        this.camera = cam;
        this.viewMatrix = Maths.createViewMatrix(camera);
        this.terrain = terrain;
    }

    public Vector3f getCurrentTerrainPoint() {
        return currentTerrainPoint;
    }

    public boolean isClicked() {
        return clicked;
    }
    
    public Vector3f getCurrentRay() {
        return currentRay;
    }

    /**
     * Method that update mouse ray on click.
     * 
     */
    public void update(){
        if (!clicked && Mouse.isButtonDown(0)){
            clicked = true;
            viewMatrix = Maths.createViewMatrix(camera);
            currentRay = calculateRay();
            if (intersectionInRange(0, RAY_RANGE, currentRay)) {
			currentTerrainPoint = binarySearch(0, 0, RAY_RANGE, currentRay);
		} else {
			currentTerrainPoint = null;
		}
        }
        if (!Mouse.isButtonDown(0)){
            clicked = false;
        }
    }
    /**
     * gets mouse coords on screen, normalizes them, adds z coordinates,
     * transform them into world depending on the camera view. Returns the 
     * world space coordinates.
     * 
     */
    private Vector3f calculateRay(){
        float mouseX = Mouse.getX();
        float mouseY = Mouse.getY();
        Vector2f normalized = getNormalized(mouseX, mouseY);
        Vector4f clipCoords = new Vector4f(normalized.x, normalized.y, -1f, 1f);
        Vector4f eyeCoords = toEyeCoords(clipCoords);
        Vector3f worldCoords = toWorldCoords(eyeCoords);
        return worldCoords;
    }
    
    /**
     * 
     * @param clipCoords
     * @return coords according to camera view
     */
    private Vector4f toEyeCoords(Vector4f clipCoords){
        Matrix4f inv = Matrix4f.invert(projectionMatrix, null);     
        Vector4f eyeCoords = Matrix4f.transform(inv, clipCoords, null);
        return new Vector4f(eyeCoords.x, eyeCoords.y, -1f, 0);
    }
    /**
     * 
     * @param eyeCoords
     * @return world space coords 
     */
    private Vector3f toWorldCoords(Vector4f eyeCoords){
        Matrix4f inv = Matrix4f.invert(viewMatrix, null);
        Vector4f worldCoords = Matrix4f.transform(inv, eyeCoords, null);
        Vector3f mouseRay = new Vector3f(worldCoords.x, worldCoords.y, worldCoords.z);
        mouseRay.normalise();
        return mouseRay;
    }
    /**
     * 
     * @param x
     * @param y
     * @return normalizes vectors into opengl world 
     */
    private Vector2f getNormalized(float x, float y){
        float X = (2f*x) / Display.getWidth() - 1;
        float Y = (2f*y) / Display.getHeight() - 1;
        return new Vector2f (X, Y);
    }
    /**
     * 
     * @param ray
     * @param distance
     * @return point on the ray
     */
    private Vector3f getPointOnRay(Vector3f ray, float distance) {
        Vector3f camPos = camera.getPosition();
        Vector3f start = new Vector3f(camPos.x, camPos.y, camPos.z);
        Vector3f scaledRay = new Vector3f(ray.x * distance, ray.y * distance, ray.z * distance);
        return Vector3f.add(start, scaledRay, null);
    }
	/**
         * Finds terrain point by binary search. That is dividing the range of
         * the ray by 2 and checking if the terrain is in the upper or in 
         * the lower part.
         * @param count
         * @param start
         * @param finish
         * @param ray
         * @return terrain point
         */
    private Vector3f binarySearch(int count, float start, float finish, Vector3f ray) {
        float half = start + ((finish - start) / 2f);
        if (count >= RECURSION_COUNT) {
                Vector3f endPoint = getPointOnRay(ray, half);
                Terrain terrain = getTerrain(endPoint.getX(), endPoint.getZ());
                if (terrain != null) {
                        return endPoint;
                } else {
                        return null;
                }
        }
        if (intersectionInRange(start, half, ray)) {
                return binarySearch(count + 1, start, half, ray);
        } else {
                return binarySearch(count + 1, half, finish, ray);
        }
    }

    /**
     * 
     * Check if terrain is in the range.
     * @param start
     * @param finish
     * @param ray
     * @return 
     */
    private boolean intersectionInRange(float start, float finish, Vector3f ray) {
        Vector3f startPoint = getPointOnRay(ray, start);
        Vector3f endPoint = getPointOnRay(ray, finish);
        if (!isUnderGround(startPoint) && isUnderGround(endPoint)) {
                return true;
        } else {
                return false;
        }
    }

    private boolean isUnderGround(Vector3f testPoint) {
        Terrain terrain = getTerrain(testPoint.getX(), testPoint.getZ());
        float height = 0;
        
        if (testPoint.y < height) {
                return true;
        } else {
                return false;
        }
    }

    private Terrain getTerrain(float worldX, float worldZ) {
        return terrain;
    }
    
}
