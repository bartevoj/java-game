package tools;

import org.lwjgl.util.vector.Vector3f;

public class Sphere {

    public static Vector3f getCoordsOnSphere(Vector3f center, float radius, double theta, double fi){
        
        fi = fi * Math.PI / 180;
        theta = theta * Math.PI / 180;
        
        float x = (float)(center.x + radius * Math.sin(fi) * Math.sin(theta));
        float y = (float)(center.y + radius * Math.cos(theta));
        float z = (float)(center.z + radius * Math.cos(fi) * Math.sin(theta));
        
        return new Vector3f(x,y,z);
    }
    
    
}
