package tools;

import entities.Camera;
import entities.Entity;
import entities.ModelOfEntities;
import entities.MouseController;
import org.lwjgl.util.vector.Vector3f;

public class GameController {
    private Camera camera;
    private MouseController mouseController;
    private Vector3f direction;
    private float distanceToRun;
    private float runnedDistance;
    private float TOLERANCE = 0.5f;
    
    private boolean shouldMove = false;
    private static Vector3f playerPos;     //local position
    private static Vector3f playerOriginalPos;
    
    public GameController(Camera camera, MouseController mouseController, ModelOfEntities player) {
        this.camera = camera;
        this.mouseController = mouseController;
        this.direction = new Vector3f(0,0,0);
        playerPos = new Vector3f(player.getEntities().get(0).getPosition());
        playerOriginalPos = new Vector3f(player.getEntities().get(0).getPosition());
        // changed to get position of first model in array in player
    }

    public static Vector3f getPlayerPos() {
        return playerPos;
    }

    public static Vector3f getPlayerOriginalPos() {
        return playerOriginalPos;
    }
    
    /**
     * Update the position of the mouse every time mouse is clicked.
     * @param player 
     */
    public void update(ModelOfEntities player){
        mouseController.update();
        if (mouseController.isClicked()){
            if (mouseController.getCurrentTerrainPoint() != null){
                getDirection(player, mouseController.getCurrentTerrainPoint());
            }
        }
        if ((distanceToRun - runnedDistance) < TOLERANCE && 
                (distanceToRun - runnedDistance) > -TOLERANCE){
            shouldMove = false;
        }
        if (shouldMove){
            camera.moveTowards(direction);
            playerPos = Vector3f.add(playerPos, direction, playerPos);
            runnedDistance += Maths.getLenght(direction);
        }
    }
    /**
     * Calculates orientation of movement, destination and sets player rotation.
     * @param player
     * @param destination 
     */
    public void getDirection(ModelOfEntities player, Vector3f destination){
        direction = Vector3f.sub(destination, playerPos, direction);       
        setPlayerRotation(player, direction);
 
        
        
        runnedDistance = 0;
        distanceToRun = Maths.getLenght(direction);
        
        shouldMove = true;
    }
    /**
     * Sets player rotation.
     * @param player entity
     * @param direction of the movement
     */
    private void setPlayerRotation(ModelOfEntities player, Vector3f direction){
        float angle = (Vector3f.angle(new Vector3f(1,0,0), direction) * 180 / (float)Math.PI);
        if (is1or2quadrant(direction)){
            angle = 180 + (180 - angle);
        }
        for(Entity entity : player.getEntities()){
            entity.setRotY(angle+90);
        }
        
    }
    /**
     * Calculates whether the direction is in the 1. or in the 2. quadrant.
     * @param direction
     * @return 
     */
    private boolean is1or2quadrant(Vector3f direction){
        float tempAngle = (Vector3f.angle(new Vector3f(0,0,-1), direction) * 180 / (float)Math.PI);
        if (tempAngle < 90){
            return true;
        }
        return false;
    }
    
    
}
