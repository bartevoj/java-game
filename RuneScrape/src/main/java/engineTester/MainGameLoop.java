package engineTester;

import entities.Camera;
import entities.Entity;
import entities.Light;
import entities.ModelOfEntities;
import entities.MouseController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.opengl.Display;
import renderEngine.DisplayManager;
import renderEngine.Loader;
import models.RawModel;
import models.TexturedModel;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.MasterRenderer;
import renderEngine.OBJLoaderSingle;
import renderEngine.OBJLoader;
import terrains.Terrain;
import textures.ModelTexture;
import textures.TerrainTexture;
import textures.TerrainTexturePack;
import tools.GameController;

public class MainGameLoop {

    public static void main(String[] args) throws IOException {

        DisplayManager.createDisplay();

        Loader loader = new Loader();

        List<Entity> entities = new ArrayList();

        Light light = new Light(new Vector3f(400, 1200, 400), new Vector3f(1.6f, 1.8f, 1.6f));

        /**
         * ************ TERRAIN **************
         */
        TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grass"));
        TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("mud"));
        TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("grassFlowers"));
        TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("path"));

        TerrainTexturePack terrainTexturePack
                = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
        TerrainTexture blendmap = new TerrainTexture(loader.loadTexture("blendMap"));

        Terrain terrain = new Terrain(0, 0, loader, terrainTexturePack, blendmap);

        /**
         * *************END OF TERRAIN **********
         */
        RawModel grass = OBJLoaderSingle.loadObjModel(loader, "grassModel");
        ModelTexture grasstext = new ModelTexture(loader.loadTexture("grassTexture"));
        TexturedModel texturedModel2 = new TexturedModel(grass, grasstext);
        ModelTexture texture2 = texturedModel2.getTexture();
        texture2.setShineDamper(1);
        texture2.setReflectivity(0);
        texture2.setHasTransparency(true);
        texture2.setUseFakeLightning(true);
        Entity entity2 = new Entity(texturedModel2, 180, 0, 180, new Vector3f(400, 0, 300), 1);

        /**
         * *********************************************
         * loading models and textures to player
         * we gotta clean this
         */
        ArrayList<Entity> modelEntities = new ArrayList<Entity>();
        ArrayList<RawModel> modelArray = OBJLoader.loadObjModel(loader, "knightv2/knight2");       
        ArrayList<ModelTexture> modelTextures = new ArrayList<ModelTexture>();
        
        for(int i = 0; i < 4; i++) {
            // boots and legs
            modelTextures.add(new ModelTexture(loader.loadTexture("textures/Metal.fw")));
        }
        modelTextures.add(new ModelTexture(loader.loadTexture("textures/red")));
        
        for(int i = 0; i < 3; i++) {
            modelTextures.add(new ModelTexture(loader.loadTexture("textures/cross")));
        }
        for(int i = 0; i < 2; i++) {
            modelTextures.add(new ModelTexture(loader.loadTexture("textures/red")));
        }
        for(int i = 0; i < 2; i++) {
            modelTextures.add(new ModelTexture(loader.loadTexture("textures/red")));
        }
        for(int i = 0; i < 3; i++) {
            // maybe sword
            modelTextures.add(new ModelTexture(loader.loadTexture("textures/silver")));
        }
        for(int i = 0; i < 2; i++) {
            modelTextures.add(new ModelTexture(loader.loadTexture("textures/red")));
        }
        for(int i = 0; i < 2; i++) {
            modelTextures.add(new ModelTexture(loader.loadTexture("textures/red")));
        }
        for(int i = 0; i < 2; i++) {
            modelTextures.add(new ModelTexture(loader.loadTexture("textures/cross")));
        }
        
        //head and helmet
        modelTextures.add(new ModelTexture(loader.loadTexture("textures/Skin.fw")));
        modelTextures.add(new ModelTexture(loader.loadTexture("textures/Metal.fw")));
        modelTextures.add(new ModelTexture(loader.loadTexture("textures/Metal.fw")));
        modelTextures.add(new ModelTexture(loader.loadTexture("textures/red")));
        
        for (int i = 0; i < modelArray.size(); ++i) {
            TexturedModel texturedModel = new TexturedModel(modelArray.get(i), modelTextures.get(i));
            ModelTexture texture = texturedModel.getTexture();
            texture.setShineDamper(1);
            texture.setReflectivity(0);
            Entity entity = new Entity(texturedModel, 180, 0, 180, new Vector3f(400, 0, 300), 1);
            entity.setIsPlayer(true);
            modelEntities.add(entity);
            
        }
        
        ModelOfEntities player = new ModelOfEntities(modelEntities);

        /**
         * *********************************************
         */
        
        
        Camera camera = new Camera(light, player);
        MasterRenderer renderer = new MasterRenderer();
        MouseController mouseController = new MouseController(renderer.getProjectionMatrix(), camera, terrain);
        GameController gameController = new GameController(camera, mouseController, player);
        while (!Display.isCloseRequested()) {
            camera.move();
            gameController.update(player);

            renderer.processTerrain(terrain);
            
            for (Entity entity : player.getEntities()) {
                renderer.processEntity(entity);
            }
            
            for (Entity entity3 : entities) {
                renderer.processEntity(entity3);
            }
            renderer.render(light, camera);
            DisplayManager.updateDisplay();
        }

        renderer.cleanUp();
        loader.cleanUp();
        DisplayManager.closeDisplay();
    }
}
